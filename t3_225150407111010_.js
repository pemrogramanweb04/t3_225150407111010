const submitBtn = document.querySelector("#submitBtn");
submitBtn.addEventListener('click', () => {
    const name = document.getElementById("name").value;
    const riwayatSakitChecked = document.querySelector('input[name="riwayat_sakit"]:checked');
    const tingkatKesadaran = document.getElementById("tingkat_kesadaran").value;

    if (name === "" || !riwayatSakitChecked || tingkatKesadaran === "") {
        alert("Harap isi semua kolom terlebih dahulu");
        return; 
    }

    const riwayatSakit = riwayatSakitChecked.value;
    const url = "http://localhost/PEMWEB/t3[225150407111010].php?name=" + name + "&riwayat_sakit=" + riwayatSakit + "&tingkat_kesadaran=" + tingkatKesadaran;

    axios.get(url)
        .then((response) => {
            alert("Hello " + name); 
            document.getElementById("nama_result").innerText = name;
            document.getElementById("riwayat_sakit_result").innerText = riwayatSakit;
            document.getElementById("tingkat_kesadaran_result").innerText = tingkatKesadaran;
            document.getElementById("result").style.display = "block";
        })
        .catch((error) => {
            console.error('Error:', error); 
        });
});
